const balance = 5;
const isActive = true;
const checkBalance = true;

if (checkBalance === false) {
  console.log("Have a nice day.");
}

else if (isActive === true && balance > 0) {
  console.log(balance);
}

else if (isActive === false && balance > 0) {
  console.log("Your account is not active");
}

else if (isActive === true && balance === 0) {
  console.log("Your account is empty");
}

else if (isActive === true && balance < 0) {
  console.log("Your balance is negative.");
}
