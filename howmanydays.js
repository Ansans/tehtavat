let kuuNro = process.argv[2];
const pvm = new Date();
const year = pvm.getFullYear();

if (kuuNro > 11) {
  kuuNro = undefined;
  console.log("Valitse numero väliltä 0-11");
}

const daysInMonth = new Date(year, kuuNro, 0).getDate();

console.log(daysInMonth);

// Tämä toimii mutten löytänyt mitään Date-ratkaisua, jossa ei olisi myös vuotta turhaan mukana. 
// Lisäksi 11:tä suuremmat numerot antavat myös vastauksen, joten estin sen noin.