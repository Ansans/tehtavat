module.exports = {
    "env": {
      "browser": false,
      "es2021": true,
      "node" : true,
    },
    "extends": [
      "eslint:recommended",
      "plugin:react/recommended"
    ],
    "parserOptions": {
      "ecmaFeatures": {
        "jsx": true 
      },
      "ecmaVersion": 13,
      "sourceType": "module"
    },
    "plugins": [
      "react"
    ],
    "rules": {
      "indent": ["error", 2], // changed from 4 to 2
      "linebreak-style": ["error", "windows"],
      "quotes": ["error", "double"],
      "semi": ["error", "always"],
      "no-var": "error",
      "no-console": 0,
    }
  
    }
  
  