const number_1 = parseInt(process.argv[2]); 
const number_2 = parseInt(process.argv[3]); 
const number_3 = parseInt(process.argv[4]);
let suurin = 0;
let pienin = 0;

if (number_3 === number_2 && number_2 === number_1) {
  console.log("They are all equal.");
}

suurin = Math.max(number_1, number_2, number_3);
console.log(`${Object.keys({suurin})[0]} ${suurin}`);

pienin = Math.min(number_1, number_2, number_3);
console.log(`${Object.keys({pienin})[0]} ${pienin}`);

